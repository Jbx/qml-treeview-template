#include "tree_item.h"
#include "tree_model.h"

TreeItem::TreeItem()
  : m_isExpanded(false)
{

}

void TreeItem::addChild(TreeItemPtr const& item)
{
  m_children.push_back(item);
  item->m_parent = shared_from_this();
  emit childAdded(item);
}

void TreeItem::removeChild(TreeItemPtr const& item)
{
  emit childRemoved(item);
  item->m_parent.reset();
  m_children.remove(item);
}

void TreeItem::clear()
{
  auto it = m_children.begin();
  for (;it != m_children.end();)
  {
    auto child = *it;
    it = m_children.erase(it);
    emit childRemoved(child);
  }
}

std::list<TreeItemPtr> const& TreeItem::getChildren() const
{
  return m_children;
}

TreeItemWeakPtr TreeItem::getParent() const
{
  return m_parent;
}

QVariant TreeItem::data(int) const
{
  return QVariant();
}

bool TreeItem::setData(QVariant const&, int)
{
  return false;
}

void TreeItem::setExpanded(bool expanded)
{
  if (m_isExpanded != expanded)
  {
    m_isExpanded = expanded;
    emit dataChanged(TreeModel::ExpandedRole);
  }
}

bool TreeItem::isExpanded() const
{
  return m_isExpanded;
}
