QT -= gui
QT += qml

TEMPLATE = lib

DEFINES += TREEVIEW_LIBRARY

CONFIG += c++14

SOURCES += \
    tree_item.cpp \
    tree_model.cpp

HEADERS += \
    tree_item_explorer.h \
    treeview_global.h \
    tree_item.h \
    tree_model.h

# Default rules for deployment.
unix {
    target.path = /usr/lib
}
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources.qrc \
    widgets.qrc
