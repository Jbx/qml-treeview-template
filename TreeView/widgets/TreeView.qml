import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.impl 2.12

ListView {
    id: root

    property color textColor: "black"    
    property int indentationWidth: 20
    property string expandIcon: "qrc:/resources/drop-indicator.png"
    property color expandIconColor: "black"
    property color expandIconHoveredColor: "cyan"
    property int expandIconSize: 16

    function createDelegate(model) {
        return undefined
    }

    ScrollBar.vertical: ScrollBar {
        policy: parent.contentHeight > parent.height ? ScrollBar.AlwaysOn : ScrollBar.AsNeeded
    }

    spacing: 0

    Component {
        id: childDelegate

        Column {
            id: node
            property var itemModel: model
            property bool expanded: model.expanded

            width: parent ? parent.width : 0
            height: expanded ? row.height + childrenView.height : row.height

            Row {
                id: row
                height: loader.height
                spacing: 0

                Item {
                    y: Math.round(row.height / 2 - height / 2)
                    width: expandIconSize
                    height: expandIconSize

                    rotation: expanded ? 0 : -90

                    ColorImage {
                        id: image
                        visible: childrenView.count > 0
                        anchors.fill: parent
                        anchors.margins: 2
                        source: root.expandIcon
                        fillMode: Image.PreserveAspectFit
                        color: mouseArea.containsMouse ? root.expandIconHoveredColor : root.expandIconColor
                    }

                    MouseArea {
                        id: mouseArea
                        anchors.fill: parent
                        hoverEnabled: true
                        onClicked: model.expanded = !expanded
                    }
                }

                Loader {
                    id: loader

                    sourceComponent: root.createDelegate(model)

                    height: item ? item.height : 0

                    property var model

                    Binding {
                        target: loader
                        property: "model"
                        value: itemModel
                        when: loader.sourceComponent !== undefined
                    }
                }
            }

            ListView {
                id: childrenView
                x: root.indentationWidth
                width: parent.width - root.indentationWidth
                height: contentHeight
                visible: expanded
                interactive: false
                spacing: 0
                model: itemModel.childModel
                delegate: childDelegate
            }
        }
    }

    delegate: childDelegate
}
