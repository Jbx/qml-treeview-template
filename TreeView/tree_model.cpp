#include "tree_model.h"
#include "tree_item_explorer.h"

// STL
#include <deque>

const int TreeModel::ChildModelRole = 0x1001;
const QByteArray TreeModel::ChildModelName = "childModel";

const int TreeModel::ExpandedRole = 0x1002;
const QByteArray TreeModel::ExpandedName = "expanded";


TreeModel::TreeModel(QHash<int, QByteArray> const& rolesNames)
  : m_rootItem(std::make_shared<TreeItem>())
  , m_rolesNames(rolesNames)
{
  m_rolesNames.insert(ChildModelRole, ChildModelName);
  m_rolesNames.insert(ExpandedRole, ExpandedName);

  connect(m_rootItem.get(), &TreeItem::childAdded, this, &TreeModel::onChildAdded);
  connect(m_rootItem.get(), &TreeItem::childRemoved, this, &TreeModel::onChildRemoved);
  m_rootItem->setExpanded(true);
}

TreeItemPtr TreeModel::rootItem() const
{
  return m_rootItem;
}

void TreeModel::expandAll()
{
  TreeExplorer<TreeItem>::exploreWidth(m_rootItem, [](auto const& treeItem) { treeItem->setExpanded(true); });
}

void TreeModel::collapseAll()
{
  TreeExplorer<TreeItem>::exploreWidth(m_rootItem, [](auto const& treeItem) { treeItem->setExpanded(false); });
}

QVariant TreeModel::data(QModelIndex const& index, int role) const
{
  if (index.row() < 0 || index.row() >= static_cast<int>(m_childModels.size()))
    return QVariant();

  auto itemAndChildModel = m_childModels[static_cast<size_t>(index.row())];

  if (role == ChildModelRole)
  {
    auto childModel = itemAndChildModel.second.get();
    return QVariant::fromValue(childModel);
  }
  else if (role == ExpandedRole)
  {
    return itemAndChildModel.first->isExpanded();
  }
  else
  {
    return itemAndChildModel.first->data(role);
  }
}

bool TreeModel::setData(QModelIndex const& index, QVariant const& value, int role)
{
  if (index.row() < 0 || index.row() >= static_cast<int>(m_childModels.size()))
    return false;

  auto itemAndChildModel = m_childModels[static_cast<size_t>(index.row())];
  auto treeItem = itemAndChildModel.first;

  auto result = false;
  if (role == ExpandedRole)
  {
    treeItem->setExpanded(value.toBool());
    result = true;
  }
  else
  {
    result = treeItem->setData(value, role);
  }

  if (result)
  {
    emit dataChanged(index, index, {role});
  }

  return result;
}

int TreeModel::rowCount(QModelIndex const&) const
{
  return static_cast<int>(m_childModels.size());
}

QHash<int, QByteArray> TreeModel::roleNames() const
{
  return m_rolesNames;
}

Qt::ItemFlags TreeModel::flags(QModelIndex const& index) const
{
  if (!index.isValid())
    return Qt::ItemIsEnabled;

  return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
}

void TreeModel::onChildAdded(TreeItemPtr const& treeItem)
{
  beginInsertRows(QModelIndex(), rowCount(), rowCount());
  m_childModels.push_back({treeItem, TreeModel::create(treeItem, m_rolesNames)});
  endInsertRows();

  connect(treeItem.get(), &TreeItem::dataChanged, [this, treeItem](int role) { this->onDataChanged(treeItem, role); });
}

void TreeModel::onChildRemoved(TreeItemPtr const& treeItem)
{
  auto it = std::find_if(
        m_childModels.begin(),
        m_childModels.end(),
        [&treeItem](auto const& child) { return child.first == treeItem; });

  if (it != m_childModels.end())
  {
    int distance = static_cast<int>(std::distance(m_childModels.begin(), it));
    beginRemoveRows(QModelIndex(), distance, distance);
    m_childModels.erase(it);
    endRemoveRows();
  }
}

void TreeModel::onDataChanged(TreeItemPtr const& treeItem, int role)
{
  auto it = std::find_if(
        m_childModels.begin(),
        m_childModels.end(),
        [&treeItem](auto const& child) { return child.first == treeItem; });

  if (it != m_childModels.end())
  {
    int distance = static_cast<int>(std::distance(m_childModels.begin(), it));
    emit dataChanged(index(distance, 0), index(distance, 0), {role});
  }
}

TreeModel::TreeModel(TreeItemPtr const& treeItem, QHash<int, QByteArray> const& rolesNames)
  : m_rootItem(treeItem)
  , m_rolesNames(rolesNames)
{
  connect(m_rootItem.get(), &TreeItem::childAdded, this, &TreeModel::onChildAdded);
  connect(m_rootItem.get(), &TreeItem::childRemoved, this, &TreeModel::onChildRemoved);
}
