#ifndef TREE_ITEM_H
#define TREE_ITEM_H

#include "treeview_global.h"

// Qt
#include <QObject>
#include <QVariant>

// STL
#include <memory>
#include <typeinfo>

class TreeItem;
using TreeItemPtr = std::shared_ptr<TreeItem>;
using TreeItemWeakPtr = std::weak_ptr<TreeItem>;

// Allow to use TreeItemPtr with Qt signals / slots
Q_DECLARE_METATYPE(TreeItemPtr)

class TREEVIEW_EXPORT TreeItem : public QObject, public std::enable_shared_from_this<TreeItem>
{
  Q_OBJECT

public:
  TreeItem();

  virtual ~TreeItem() = default;

  void addChild(TreeItemPtr const& child);
  void removeChild(TreeItemPtr const& child);

  template<typename TreeItemDerived, typename ...Args>
  std::shared_ptr<TreeItemDerived> createChild(Args&&...args)
  {
    static_assert(std::is_base_of<TreeItem, TreeItemDerived>::value, "TreeItemDerived class must inherit from TreeItem class");
    auto treeItem = std::make_shared<TreeItemDerived>(std::forward<Args>(args)...);
    addChild(treeItem);
    return treeItem;
  }

  void clear();

  std::list<TreeItemPtr> const& getChildren() const;
  TreeItemWeakPtr getParent() const;

  virtual QVariant data(int role) const;
  virtual bool setData(QVariant const& value, int role);

  void setExpanded(bool expanded);
  bool isExpanded() const;

signals:
  void dataChanged(int role);
  void childAdded(TreeItemPtr const& child);
  void childRemoved(TreeItemPtr const& child);

private:
  TreeItemWeakPtr m_parent;
  std::list<TreeItemPtr> m_children;
  bool m_isExpanded;
};

#endif // TREE_ITEM_H
