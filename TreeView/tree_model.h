#ifndef TREE_MODEL_H
#define TREE_MODEL_H

#include "tree_item.h"

// Qt
#include <QAbstractListModel>

// STL
#include <memory>

class TREEVIEW_EXPORT TreeModel : public QAbstractListModel
{
  Q_OBJECT

public:
  TreeModel(QHash<int, QByteArray> const& rolesNames);

  TreeItemPtr rootItem() const;

  Q_INVOKABLE void expandAll();
  Q_INVOKABLE void collapseAll();

protected:
  // QAbstractListModel implementation
  virtual QVariant data(QModelIndex const& index, int role) const final;
  virtual bool setData(QModelIndex const& index, QVariant const& value, int role) final;
  virtual int rowCount(QModelIndex const& = QModelIndex()) const final;
  virtual QHash<int, QByteArray> roleNames() const final;
  virtual Qt::ItemFlags flags(QModelIndex const& index) const final;

private slots:
  void onChildAdded(TreeItemPtr const& child);
  void onChildRemoved(TreeItemPtr const& child);

private:
  void onDataChanged(TreeItemPtr const& child, int role);

private:
  friend class TreeItem;

  // Allow to use std::make_shared with private ctor
  template<typename ...Args>
  static std::shared_ptr<TreeModel> create(Args&&...args)
  {
    struct EnableMakeShared : public TreeModel
    {
      EnableMakeShared(Args&&...args) : TreeModel(std::forward<Args>(args)...) {}
    };
    return std::make_shared<EnableMakeShared>(std::forward<Args>(args)...);
  }

  TreeModel(TreeItemPtr const& node, QHash<int, QByteArray> const& rolesNames);

  // Reserved role to manage child model
  static const int ChildModelRole;
  static const QByteArray ChildModelName;

  // Reserved role to manage expanded
  static const int ExpandedRole;
  static const QByteArray ExpandedName;

  QHash<int, QByteArray> createRoleNames() const;

  TreeItemPtr m_rootItem;
  std::vector<std::pair<TreeItemPtr, std::shared_ptr<TreeModel>>> m_childModels;
  QHash<int, QByteArray> m_rolesNames;

};

#endif // TREE_MODEL_H
