#ifndef TREE_ITEM_EXPLORER_H
#define TREE_ITEM_EXPLORER_H

#include "tree_item.h"

// STL
#include <functional>
#include <deque>

template <typename DerivedTreeItem>
class TreeExplorer
{
public:
  static void exploreWidth(
      TreeItemPtr const& root,
      std::function<void(std::shared_ptr<DerivedTreeItem> const&)> const& onTreeItem)
  {
    std::deque<TreeItemPtr> deque;
    deque.push_back(root);

    while (!deque.empty())
    {
      auto current = deque.front();
      deque.pop_front();

      onTreeItem(std::static_pointer_cast<DerivedTreeItem>(current));

      for (auto const& child : current->getChildren())
      {
        deque.push_back(child);
      }
    }
  }

  static void exploreDepth(
      TreeItemPtr const& root,
      std::function<void(std::shared_ptr<DerivedTreeItem> const&)> const& onTreeItem)
  {
    std::deque<TreeItemPtr> deque;
    deque.push_back(root);

    while (!deque.empty())
    {
      auto current = deque.front();
      deque.pop_front();

      onTreeItem(std::static_pointer_cast<DerivedTreeItem>(current));

      auto children = current->getChildren();
      if (!children.empty())
      {
        deque.insert(deque.begin(), children.begin(), children.end());
      }
    }
  }
};

#endif // TREE_ITEM_EXPLORER_H
