QT += quick qml

CONFIG += c++14

SOURCES += \
    app_controller.cpp \
    main.cpp

HEADERS += \
    app_controller.h

RESOURCES += qml.qrc

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../TreeView/release/ -lTreeView
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../TreeView/debug/ -lTreeView
else:unix: LIBS += -L$$OUT_PWD/../TreeView/ -lTreeView

INCLUDEPATH += $$PWD/../TreeView
DEPENDPATH += $$PWD/../TreeView
