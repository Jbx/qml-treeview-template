import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.5

import "qrc:/widgets/" as QmlWidgets

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    QmlWidgets.TreeView {
        id: treeView
        anchors.fill: parent

        model: appController.treeModel

        Component {
            id: buttonComponent

            Button {
                width: 180
                height: 20
                text: model.name

                onClicked: {
                    model.selected = !model.selected
                }

                Rectangle {
                    width: 10
                    height: 10

                    anchors.right: parent.right
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.margins: 4

                    color: model.selected ? "black" : "white"
                }
            }
        }

        Component {
            id: textFieldComponent

            TextField {
                width: 180
                height: 30
                text: model.name
            }
        }

        function createDelegate(model) {
            switch (model.type) {
            case 0:
                return textFieldComponent
            case 1:
                return buttonComponent
            }
        }
    }

    Button {
        id: expandAllButton
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.margins: 4
        width: 100

        text: "Expand all"

        onClicked: {
            appController.treeModel.expandAll()
        }
    }

    Button {
        id: collapseAllButton
        anchors.top: expandAllButton.bottom
        anchors.right: parent.right
        anchors.margins: 4
        width: 100

        text: "Collaspe all"

        onClicked: {
            appController.treeModel.collapseAll()
        }
    }

    Button {
        id: changeRootName
        anchors.top: collapseAllButton.bottom
        anchors.right: parent.right
        anchors.margins: 4
        width: 100

        text: "Change root name"

        onClicked: {
            appController.setRootName("New name")
        }
    }

    Button {
        id: addItem
        anchors.top: changeRootName.bottom
        anchors.right: parent.right
        anchors.margins: 4
        width: 100

        text: "Add item"

        onClicked: {
            appController.addItem("New item")
        }
    }

    Button {
        id: addItem100
        anchors.top: addItem.bottom
        anchors.right: parent.right
        anchors.margins: 4
        width: 100

        text: "Add item x 100"

        onClicked: {
            appController.add100Item("New item")
        }
    }

    Button {
        id: removeItem
        anchors.top: addItem100.bottom
        anchors.right: parent.right
        anchors.margins: 4
        width: 100

        text: "Remove item"

        onClicked: {
            appController.removeItem()
        }
    }

    Button {
        id: selectAll
        anchors.top: removeItem.bottom
        anchors.right: parent.right
        anchors.margins: 4
        width: 100

        text: "Select All"

        onClicked: {
            appController.selectAll()
        }
    }

    Button {
        id: unselectAll
        anchors.top: selectAll.bottom
        anchors.right: parent.right
        anchors.margins: 4
        width: 100

        text: "Unselect All"

        onClicked: {
            appController.unselectAll()
        }
    }
}
