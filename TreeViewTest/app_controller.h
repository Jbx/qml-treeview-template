#ifndef APPCONTROLLER_H
#define APPCONTROLLER_H

#include "tree_model.h"

enum MyRoles
{
  NameRole = Qt::UserRole + 1,
  SelectedRole,
  VisibleRole,
  TypeRole
};

class ATreeItem : public TreeItem
{
public:
  virtual void setName(std::string const& name) = 0;
  virtual std::string const& getName() const = 0;
  virtual void setSelected(bool selected) = 0;
  virtual bool isSelected() const = 0;
  virtual void setVisible(bool visible) = 0;
  virtual bool isVisible() const = 0;
  virtual int getType() const = 0;

  virtual QVariant data(int role) const override
  {
    if (role == NameRole)
      return QString::fromStdString(getName());
    else if (role == SelectedRole)
      return isSelected();
    else if (role == VisibleRole)
      return isVisible();
    else if (role == TypeRole)
      return getType();
    return QVariant();
  }

  virtual bool setData(QVariant const& value, int role) override
  {
    if (role == NameRole)
      setName(value.toString().toStdString());
    else if (role == SelectedRole)
      setSelected(value.toBool());
    else if (role == VisibleRole)
      setVisible(value.toBool());
    else return false;

    return true;
  }
};


class NodeTreeItem : public ATreeItem
{
public:
  NodeTreeItem(std::string const& name)
    : m_name(name)
    , m_selected(false)
    , m_visible(true) {}

  virtual void setName(std::string const& name) override
  {
    if (m_name != name)
    {
      m_name = name;
      emit dataChanged(NameRole);
    }
  }

  virtual std::string const& getName() const override { return m_name; }

  virtual void setSelected(bool selected) override
  {
    if (m_selected != selected)
    {
      m_selected = selected;
      emit dataChanged(SelectedRole);
    }
  }

  virtual bool isSelected() const override { return m_selected; }

  virtual void setVisible(bool visible) override
  {
    if (m_visible != visible)
    {
      m_visible = visible;
      emit dataChanged(VisibleRole);
    }
  }

  virtual bool isVisible() const override { return m_visible; }

  virtual int getType() const override { return 0; }

private:
  std::string m_name;
  bool m_selected;
  bool m_visible;
};

class GeometryTreeItem : public ATreeItem
{
public:
  GeometryTreeItem(std::string const& name)
    : m_name(name)
    , m_selected(false)
    , m_visible(true) {}

  virtual void setName(std::string const& name) override
  {
    if (m_name != name)
    {
      m_name = name;
      emit dataChanged(NameRole);
    }
  }

  virtual std::string const& getName() const override { return m_name; }

  virtual void setSelected(bool selected) override
  {
    if (m_selected != selected)
    {
      m_selected = selected;
      emit dataChanged(SelectedRole);
    }
  }

  virtual bool isSelected() const override { return m_selected; }

  virtual void setVisible(bool visible) override
  {
    if (m_visible != visible)
    {
      m_visible = visible;
      emit dataChanged(VisibleRole);
    }
  }

  virtual bool isVisible() const override { return m_visible; }

  virtual int getType() const override { return 1; }

private:
  std::string m_name;
  bool m_selected;
  bool m_visible;
};


class AppController : public QObject
{
  Q_OBJECT
  Q_PROPERTY(QObject* treeModel READ getTreeModel CONSTANT)

public:
  AppController();

  Q_INVOKABLE void setRootName(QString const& name);

  Q_INVOKABLE void addItem(QString const& name);
  Q_INVOKABLE void add100Item(QString const& name);
  Q_INVOKABLE void removeItem();

  Q_INVOKABLE void selectAll();
  Q_INVOKABLE void unselectAll();

  TreeModel* getTreeModel();

private:
  TreeModel m_treeModel;
  std::shared_ptr<ATreeItem> m_item;
};

#endif // APPCONTROLLER_H
