#include "app_controller.h"
#include "tree_item_explorer.h"

AppController::AppController()
  : m_treeModel({
  {NameRole, "name"},
  {SelectedRole, "selected"},
  {VisibleRole, "visible"},
  {TypeRole, "type"}
})
{
  m_item = m_treeModel.rootItem()->createChild<NodeTreeItem>("Root");
  auto child2 = m_item->createChild<NodeTreeItem>("Node1");
  child2->createChild<NodeTreeItem>("SubNode1");
  auto child3 = m_item->createChild<NodeTreeItem>("Node2");
  auto child4 = child3->createChild<NodeTreeItem>("SubNode2");
  child4->createChild<NodeTreeItem>("SubSubNode4");
  child4->createChild<GeometryTreeItem>("SubSubNode4");
}

void AppController::setRootName(QString const& name)
{
  m_item->setName(name.toStdString());
}

void AppController::addItem(QString const& name)
{
  auto item = std::make_shared<NodeTreeItem>(name.toStdString());
  m_item->addChild(item);
}

void AppController::add100Item(QString const& name)
{
  static int count = 0;
  for (int i = 0; i < 100; ++i)
  {
    auto child = m_item->createChild<NodeTreeItem>(name.toStdString() + " " + std::to_string(count++));
    auto subChild = child->createChild<NodeTreeItem>("Sub" + name.toStdString() + " " + std::to_string(count++));
    subChild->createChild<NodeTreeItem>("SubSub" + name.toStdString() + " " + std::to_string(count++));
  }
}

void AppController::removeItem()
{
  auto const& children = m_item->getChildren();
  if (!children.empty())
  {
    m_item->removeChild(children.back());
  }
}

void AppController::selectAll()
{
  TreeExplorer<ATreeItem>::exploreWidth(m_item, [](auto const& treeItem) { treeItem->setSelected(true); });
}

void AppController::unselectAll()
{
  TreeExplorer<ATreeItem>::exploreWidth(m_item, [](auto const& treeItem) { treeItem->setSelected(false); });
}

TreeModel* AppController::getTreeModel()
{
  return &m_treeModel;
}
